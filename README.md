# NexusSlider Project Container (~ Angular Components)

[![pipeline status](https://gitlab.com/lorenzocalamandrei/ngx-nexus-slider/badges/master/pipeline.svg)](https://gitlab.com/lorenzocalamandrei/ngx-nexus-slider/commits/master)
[![coverage report](https://gitlab.com/lorenzocalamandrei/ngx-nexus-slider/badges/master/coverage.svg)](https://gitlab.com/lorenzocalamandrei/ngx-nexus-slider/commits/master)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Development Info

The project structure follow the Angular library standard.

So the `nexus-slider` is place under `projects/nexus-slider`.

## Build the Nexus Slider

Run `yarn build:nexus-slider` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Author

* Lorenzo Calamandrei <nexcal.dev@gmail.com>
