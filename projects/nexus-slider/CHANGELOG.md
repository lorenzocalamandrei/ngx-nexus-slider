# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.0] - 2019-11-14
### Added
- Keywords in `nexus-slider` package.json.
### Changed
- Correct tests
- Update image for tests in gitlab-ci
- Bump version to the first major
- Update README

## [0.0.2] - 2019-11-12
### Added
- NPM License and GitLab repository url.

## [0.0.1] - 2019-11-12
### Added
- Initial module deployment.
