# NexusSlider (ngx-nexus-slider)

[![pipeline status](https://gitlab.com/lorenzocalamandrei/ngx-nexus-slider/badges/master/pipeline.svg)](https://gitlab.com/lorenzocalamandrei/ngx-nexus-slider/commits/master)
[![coverage report](https://gitlab.com/lorenzocalamandrei/ngx-nexus-slider/badges/master/coverage.svg)](https://gitlab.com/lorenzocalamandrei/ngx-nexus-slider/commits/master)

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.2.0.

## Installing

Run `yarn add ngx-nexus-slider` to install this package.

![Demo GIF](https://gitlab.com/lorenzocalamandrei/ngx-nexus-slider/raw/master/projects/nexus-slider/.readme/demo.gif)

## Usage example

```html
<nexus-slider>
    <nexus-slide>Slide 1</nexus-slide>
    <nexus-slide>Slide 2</nexus-slide>
  </nexus-slider>
```

### Possible @Input

```typescript
export class NexusSliderComponent {
    @Input() pauseOnHover = true;
    @Input() isInPause = false;
    @Input() slideActiveIndex = 0;
    @Input() slideDelay = 2000;
    
    @Input() showNavigation = true;
    @Input() showPrevNext = false;
    
    @Output() slideChange = new EventEmitter();
    @Output() goInPause = new EventEmitter();
    @Output() goInStart = new EventEmitter();
    @Output() mouseIsHover = new EventEmitter();
}
```

- `pauseOnHover`: When the user mouse is hover the slider it will be paused.
- `isInPause`: The slider will start as paused, and after the mouse pass hover the slider it will start automatic slide
- `slideActiveIndex`: Index of the slide at start point.
- `slideDelay`: Time between automatic slide.

- `showNavigation`: Show the button under the slider
- `showPrevNext`: Show the lateral arrows to go to the next or the prev.

- `slideChange`: Emit when the active slide change.
- `goInPause`: Emit when the slider is paused.
- `goInStart`: Emit when the slider is resumed.
- `mouseIsHover`: Emit when the user mouse is hover the slider.

## Code scaffolding

Run `ng generate component component-name --project nexus-slider` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project nexus-slider`.
> Note: Don't forget to add `--project nexus-slider` or else it will be added to the default project in your `angular.json` file. 

## Build

Run `yarn build:nexus-slider` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `yarn build:nexus-slider`, go to the dist folder `cd dist/nexus-slider` and run `npm publish`.

## Running unit tests

Run `ng test nexus-slider` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Author

* Lorenzo Calamandrei <nexcal.dev@gmail.com>
