import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NexusSlideComponent } from './nexus-slide.component';
import { MatTabsModule } from '@angular/material/tabs';

describe('SlideComponent', () => {
  let component: NexusSlideComponent;
  let fixture: ComponentFixture<NexusSlideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NexusSlideComponent],
      imports: [MatTabsModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NexusSlideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
