import { Component, Input, TemplateRef, ViewChild } from '@angular/core';

@Component({
  selector: 'nexus-slide',
  templateUrl: './nexus-slide.component.html',
  styleUrls: ['./nexus-slide.component.scss']
})
export class NexusSlideComponent {
  @Input() label = '';
  @ViewChild('content', { static: false }) content: TemplateRef<any>;

  constructor() {}
}
