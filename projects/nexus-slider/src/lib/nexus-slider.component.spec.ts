import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NexusSliderComponent } from './nexus-slider.component';
import { MatTabsModule } from '@angular/material/tabs';
import { MatIconModule } from '@angular/material/icon';

describe('NexusSliderComponent', () => {
  let component: NexusSliderComponent;
  let fixture: ComponentFixture<NexusSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NexusSliderComponent],
      imports: [MatTabsModule, MatIconModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NexusSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
