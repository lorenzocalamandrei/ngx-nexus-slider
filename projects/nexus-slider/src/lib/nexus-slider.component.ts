import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ContentChildren,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  QueryList,
  ViewEncapsulation
} from '@angular/core';
import { NexusSlideComponent } from './components/slide/nexus-slide.component';

@Component({
  selector: 'nexus-slider',
  templateUrl: './nexus-slider.component.html',
  styleUrls: ['./nexus-slider.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NexusSliderComponent implements OnInit, AfterViewInit, OnDestroy {
  slidesInterval: any;

  @Input() pauseOnHover = true;
  @Input() isInPause = false;
  @Input() slideActiveIndex = 0;
  @Input() slideDelay = 2000;

  @Input() showNavigation = true;
  @Input() showPrevNext = false;

  @Output() slideChange = new EventEmitter();
  @Output() goInPause = new EventEmitter();
  @Output() goInStart = new EventEmitter();
  @Output() mouseIsHover = new EventEmitter();

  @ContentChildren(NexusSlideComponent) slides: QueryList<NexusSlideComponent>;

  constructor(private changeDetectorRef: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.slidesInterval = setInterval(
      () => this.goToTheNextSlide(),
      this.slideDelay
    );
  }

  ngAfterViewInit(): void {
    this.changeDetectorRef.detectChanges();
  }

  ngOnDestroy(): void {
    clearInterval(this.slidesInterval);
  }

  pauseSlider(): void {
    this.isInPause = true;
    this.goInPause.emit();
  }

  resumeSlider(): void {
    this.isInPause = false;
    this.goInStart.emit();
  }

  onSliderHoverIn(): void {
    if (this.pauseOnHover) {
      this.pauseSlider();
    }
    this.mouseIsHover.emit();
  }

  onSliderHoverOut(): void {
    this.resumeSlider();
  }

  goToTheNextSlide(): void {
    if (this.isInPause) {
      return;
    }

    if (this.slideActiveIndex + 1 < this.slides.length) {
      this.slideActiveIndex++;
      this.slideChange.emit(this.slideActiveIndex);
      return;
    }

    this.slideActiveIndex = 0;
    this.slideChange.emit(this.slideActiveIndex);
  }

  nextSlide() {
    if (this.slideActiveIndex + 1 < this.slides.length) {
      this.slideActiveIndex++;
      this.slideChange.emit(this.slideActiveIndex);
      return;
    }

    this.slideActiveIndex = 0;
    this.slideChange.emit(this.slideActiveIndex);
  }

  prevSlide() {
    if (this.slideActiveIndex - 1 >= 0) {
      this.slideActiveIndex--;
      this.slideChange.emit(this.slideActiveIndex);
      return;
    }

    this.slideActiveIndex = this.slides.length - 1;
    this.slideChange.emit(this.slideActiveIndex);
  }

  goToSlideNumber(index: number): void {
    this.slideActiveIndex = index;
    this.slideChange.emit(this.slideActiveIndex);
  }
}
