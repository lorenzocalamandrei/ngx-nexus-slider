import { NgModule } from '@angular/core';
import { NexusSliderComponent } from './nexus-slider.component';
import { NexusSlideComponent } from './components/slide/nexus-slide.component';
import { CommonModule } from '@angular/common';
import {
  MatButtonModule,
  MatIconModule,
  MatTabsModule
} from '@angular/material';

@NgModule({
  declarations: [NexusSliderComponent, NexusSlideComponent],
  imports: [CommonModule, MatTabsModule, MatIconModule, MatButtonModule],
  exports: [NexusSliderComponent, NexusSlideComponent]
})
export class NexusSliderModule {}
