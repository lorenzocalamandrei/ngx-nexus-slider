import { TestBed } from '@angular/core/testing';

import { NexusSliderService } from './nexus-slider.service';

describe('NexusSliderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NexusSliderService = TestBed.get(NexusSliderService);
    expect(service).toBeTruthy();
  });
});
