/*
 * Public API Surface of nexus-slider
 */

export * from './lib/nexus-slider.service';
export * from './lib/nexus-slider.component';
export * from './lib/nexus-slider.module';
